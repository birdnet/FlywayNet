#include <Rcpp.h>
#include <Rcpp/Benchmark/Timer.h>

#include "birdnet_mcem_utils.h"
#include "birdnet_mcem_sis.h"
#include "birdnet_mcem_mh.h"
#include "generate_trajectories_C.h"
#include "get_counts_C.h"
#include "get_observation_probability_C.h"

// [[Rcpp::export]]
Rcpp::List estimate_migration_MCEM_C(
    Rcpp::List migration,
    bool estimate_transitions = false, 
    Rcpp::LogicalVector estimate_sojourns = Rcpp::LogicalVector::create(),
    bool estimate_observation_param = false,
    Rcpp::List sojourn_domain = Rcpp::List::create(),
    Rcpp::NumericMatrix start_transitions = Rcpp::NumericMatrix::zeros(1),
    Rcpp::NumericVector start_sojourns = Rcpp::NumericVector::create(),
    double start_observation_param = -1,
    int nb_particles = -1, 
    int itermax = -1,
    Rcpp::String MC_algo = "",
    double MH_neighborhood = -1,
    int MH_transition_length = -1,
    int MH_burnin = 0,
    bool log_transitions = false,
    bool log_sojourns = false,
    bool log_observation_parameter = false,
    bool log_loglikelihood = false,
    bool log_sel_particles = false,
    bool log_acceptance_rate = false,
    bool verbose = false)
  {
  
  Rcpp::Timer timer;
  bool MC_SIS = false;
  if (MC_algo == "SIS") {
    MC_SIS = true;
  } 
  std::string observation_law_type = Rcpp::as<std::string>(
    migration["observation_law_type"]);
  bool optim_delta = (observation_law_type == "NegBin_p");

  //migration structure
  Rcpp::NumericVector initial_state = migration["initial_state"];
  int horizon_time = Rcpp::as<int>(migration["horizon"]) ;
  int horizon_col = horizon_time + 1 ;
  int bird_nb = Rcpp::sum(initial_state);
  Rcpp::List curr_migration = Rcpp::clone(migration);
  curr_migration["transition_law_param"] = Rcpp::clone(start_transitions);
  curr_migration["sojourn_law_param"] = Rcpp::clone(start_sojourns);
  
  if (estimate_observation_param) {
    Rcpp::List obs_params = curr_migration["observation_law_param"];
    if (observation_law_type == "NegBin_p"){
      obs_params["delta"] = start_observation_param;
    } else if (observation_law_type == "NegBin_delta"){
      obs_params["p"] = start_observation_param;
    }
  }
  Rcpp::List logged_transitions;
  Rcpp::List logged_sojourns;
  Rcpp::List logged_observation_parameter;
  Rcpp::List logged_loglikelihood;
  Rcpp::List logged_sel_particles;
  Rcpp::List logged_acceptance_rate;
  if (log_transitions) {
    Rcpp::NumericMatrix tolog = curr_migration["transition_law_param"];
    logged_transitions.push_back(Rcpp::clone(tolog));
  }
  if (log_sojourns) {
    Rcpp::NumericVector tolog = curr_migration["sojourn_law_param"];
    logged_sojourns.push_back(Rcpp::clone(tolog));
  }
  if (log_observation_parameter) {
    Rcpp::List obs_params = curr_migration["observation_law_param"];
    if (optim_delta) {
      Rcpp::NumericVector tolog = obs_params["delta"];
      logged_observation_parameter.push_back(Rcpp::clone(tolog));
    } else {
      Rcpp::NumericVector tolog = obs_params["p"];
      logged_observation_parameter.push_back(Rcpp::clone(tolog));
    }
    
  }
  if (log_acceptance_rate) {
    logged_acceptance_rate.push_back(0.0);
  }
  
  if (MC_SIS) {//////////////////////////////////////// ////////////  SIS resampling
    bool use_log = true;
    int iter = 1;
    bool done = false;
    Rcpp::NumericMatrix particles_1(bird_nb*nb_particles, horizon_col);
    Rcpp::NumericMatrix particles_2(bird_nb*nb_particles, horizon_col);
    Rcpp::NumericMatrix obsProbs_1(nb_particles, horizon_col);
    Rcpp::NumericMatrix obsProbs_2(nb_particles, horizon_col);
    Rcpp::IntegerVector resampling(nb_particles);
    while (!done) {
      if (verbose) {Rcpp::Rcout << "-------------- ITER EM SIS " << iter << "\n";}
  
      init_observation_probabilities(obsProbs_1, use_log);
      init_observation_probabilities(obsProbs_2, use_log);
      init_particles(particles_1, curr_migration, nb_particles, 0);
      if (log_sel_particles) {
        Rcpp::NumericVector tolog(horizon_col, 1.0);
        logged_sel_particles.push_back(Rcpp::clone(tolog));
      }
      bool use_1 = true;
      for (int t=1; t<horizon_col; t++) {
        Rcpp::NumericMatrix particles_sim;
        Rcpp::NumericMatrix particles_resampled;
        Rcpp::NumericMatrix obsProbs_sim;
        Rcpp::NumericMatrix obsProbs_resampled;
        if (use_1) {
          particles_sim = particles_1;
          particles_resampled = particles_2;
          obsProbs_sim = obsProbs_1;
          obsProbs_resampled = obsProbs_2;
        } else {
          particles_sim = particles_2;
          particles_resampled = particles_1;
          obsProbs_sim = obsProbs_2;
          obsProbs_resampled = obsProbs_1;
        }
        // Sample particles
        simulate_trajectories(particles_sim, curr_migration, t);
        // Update observation probabilities
        compute_observation_probabilities(obsProbs_sim, use_log, curr_migration, 
                            particles_sim, t, bird_nb, nb_particles);
        
        // Resample trajectories
        Rcpp::IntegerVector resampling = resampling_particles(
          obsProbs_sim, use_log, t, nb_particles);
        
        if (log_sel_particles) {
          Rcpp::NumericVector tolog = 
            logged_sel_particles[logged_sel_particles.length()-1];
          Rcpp::IntegerVector unique_part = Rcpp::unique(resampling);
          tolog[t] = (double) unique_part.length() / (double) nb_particles;
        }
        
        apply_resampling(particles_resampled, obsProbs_resampled, resampling,
                         particles_sim, obsProbs_sim, bird_nb, t, 
                         nb_particles);
        use_1 = not use_1;
      }
      ////////// Update parameters
      Rcpp::NumericMatrix particles_final;
      if (use_1) {
        particles_final = particles_2;
      } else {
        particles_final = particles_1;
      }
      //TODO should provide and compute the loglikelihood of parameters on
      // particle_final
      
      update_parameters(curr_migration, particles_final, nb_particles,
                        estimate_transitions, estimate_sojourns, 
                        estimate_observation_param, optim_delta, 
                        sojourn_domain, 0.05);
      
      if (log_transitions) {
        Rcpp::NumericMatrix tolog = curr_migration["transition_law_param"];
        logged_transitions.push_back(Rcpp::clone(tolog));
      }
      if (log_sojourns) {
        Rcpp::NumericVector tolog = curr_migration["sojourn_law_param"];
        logged_sojourns.push_back(Rcpp::clone(tolog));
      }
      if (log_observation_parameter) {
        Rcpp::List obs_params = curr_migration["observation_law_param"];
        if (optim_delta) {
          Rcpp::NumericVector tolog = obs_params["delta"];
          logged_observation_parameter.push_back(Rcpp::clone(tolog));
        } else {
          Rcpp::NumericVector tolog = obs_params["p"];
          logged_observation_parameter.push_back(Rcpp::clone(tolog));
        }
      }
      if (iter >= itermax) {
        done = true;
      }
      iter ++;
    }
  } else { ///////////////////////////////////////////////////////////////////////  MH resampling
    bool use_log = true;
    int iter = 1;
    bool done = false;
    
    timer.step("MHstart");
    
    while (!done) {
        
      if (verbose) {Rcpp::Rcout << "-------------- ITER EM MH " << iter << "\n";}
      
     
      // monte carlo
      Rcpp::NumericMatrix particles_chain(bird_nb*nb_particles, horizon_col);
      std::fill(particles_chain.begin(), particles_chain.end(), 
                Rcpp::NumericMatrix::get_na()) ;
      
      Rcpp::NumericMatrix pi_curr = init_trajectories(curr_migration);
      simulate_trajectories(pi_curr, curr_migration, horizon_time);
      double log_PO_pi_curr = get_observation_probability_C(curr_migration, 
                                               get_counts_C(curr_migration, pi_curr), 
                                               use_log);
      double log_likelihood = log_PO_pi_curr;
                               //compute loglikelihood of parameters based on 
                               //maxlog operator
      int acceptance_rate = 0;
      for (int p=0;  p<MH_burnin+nb_particles; p++) {
        Rcpp::NumericVector draws = Rcpp::runif(MH_transition_length);
        for (int l=0;  l<MH_transition_length; l++) {
          Rcpp::NumericMatrix pi_prop = Rcpp::clone(pi_curr);
          resimulate_trajectories(pi_prop, curr_migration, horizon_time, 
                                  MH_neighborhood);
          double log_PO_pi_prop = get_observation_probability_C(curr_migration, 
                                             get_counts_C(curr_migration, pi_prop), 
                                             use_log);
          if (draws[l] < std::min(1.0, 
                                  std::exp(log_PO_pi_prop - log_PO_pi_curr))) {
            acceptance_rate ++;
            pi_curr = pi_prop;
            log_PO_pi_curr = log_PO_pi_prop;
          }
        }
        if (p >= MH_burnin) {
          for (int i=0; i<bird_nb; i++) {
            for (int t=0; t<horizon_col;t++){
              particles_chain(i+(p-MH_burnin)*bird_nb, t) = pi_curr(i,t);
            }
          }
          log_likelihood = maxlog(log_likelihood, log_PO_pi_curr);
          if (p == MH_burnin+nb_particles-1){
            log_likelihood = log(1.0/(double)nb_particles)+log_likelihood;
          }
        }
      }
      timer.step("MH-E-MC");
      update_parameters(curr_migration, particles_chain, nb_particles,
                        estimate_transitions, estimate_sojourns,
                        estimate_observation_param, optim_delta, 
                        sojourn_domain, 0.05);
      timer.step("MH-M");
      if (log_transitions) {
        Rcpp::NumericMatrix tolog = curr_migration["transition_law_param"];
        logged_transitions.push_back(Rcpp::clone(tolog));
      }
      if (log_sojourns) {
        Rcpp::NumericVector tolog = curr_migration["sojourn_law_param"];
        logged_sojourns.push_back(Rcpp::clone(tolog));
      }
      if (log_observation_parameter) {
        Rcpp::List obs_params = curr_migration["observation_law_param"];
        if (optim_delta) {
          Rcpp::NumericVector tolog = obs_params["delta"];
          logged_observation_parameter.push_back(Rcpp::clone(tolog));
        } else {
          Rcpp::NumericVector tolog = obs_params["p"];
          logged_observation_parameter.push_back(Rcpp::clone(tolog));
        }
      }
      if (log_loglikelihood) {
        logged_loglikelihood.push_back((double) log_likelihood);
      }
      if (log_acceptance_rate) {
        logged_acceptance_rate.push_back((double) acceptance_rate / 
          ((double) nb_particles * (double) MH_transition_length) );
      }
      if (iter >= itermax) {
        done = true;
      }
      iter ++;
    }
  }

  Rcpp::List estimation_method;
  estimation_method.push_back("MCEM", "name");
  Rcpp::List output;
  if (estimate_transitions) {
    output.push_back(curr_migration["transition_law_param"], "transition_law_param");
  } 
  if (estimate_sojourns) {
    output.push_back(curr_migration["sojourn_law_param"], "sojourn_law_param");
  }
  if (estimate_observation_param) {
    output.push_back(curr_migration["observation_law_param"], "observation_law_param");
  }
  if (log_transitions) {
    output.push_back(logged_transitions, "log_transitions");
  }
  if (log_sojourns) {
    output.push_back(logged_sojourns, "log_sojourns");
  }
  if (log_observation_parameter) {
    if (optim_delta) {
      output.push_back(logged_observation_parameter, "log_observation_parameter_delta");
    } else {
      output.push_back(logged_observation_parameter, "log_observation_parameter_p");
    }
  }
  if (log_loglikelihood) {
    output.push_back(logged_loglikelihood, "log_loglikelihood");
  }
  if (log_sel_particles) {
    output.push_back(logged_sel_particles, "log_sel_particles");
  }
  if (log_acceptance_rate) {
    output.push_back(logged_acceptance_rate, "log_acceptance_rate");
  }  
  estimation_method.push_back(output, "output");
  estimation_method.push_back(timer, "timer");
  migration.push_back(estimation_method, "estimation_method");
  return migration;
}



