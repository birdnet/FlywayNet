#include <Rcpp.h>

#include "birdnet_mcem_utils.h"




// [[Rcpp::export]]
Rcpp::List estimate_migration_from_trajectories_C(
  Rcpp::List migration,
  Rcpp::NumericMatrix trajectories, 
  bool estimate_transitions = false, 
  Rcpp::LogicalVector estimate_sojourns = Rcpp::LogicalVector::create(),
  bool estimate_observation_param=false, 
  Rcpp::List sojourn_domain = Rcpp::List::create(),
  double min_prob = 0)
{
  Rcpp::List migr = Rcpp::clone(Rcpp::as<Rcpp::List>(migration));
  bool optim_delta = false;
  if (Rcpp::as<std::string>(migration["observation_law_type"]) == "NegBin_p"){
    optim_delta = estimate_observation_param;
  }
  
  update_parameters(migr, trajectories, 1, estimate_transitions, 
                    estimate_sojourns, estimate_observation_param, 
                    optim_delta, sojourn_domain, min_prob);

  
  Rcpp::List estimation_method;
  estimation_method.push_back("from_trajectories", "name");
  Rcpp::List settings;
  settings.push_back(estimate_transitions, "estimate_transitions");
  settings.push_back(estimate_sojourns, "estimate_sojourns");
  settings.push_back(estimate_observation_param, "estimate_observation_param");
  settings.push_back(sojourn_domain, "sojourn_domain");
  settings.push_back(min_prob, "min_prob");
  estimation_method.push_back(settings, "settings");
  Rcpp::List output;
  output.push_back(migr["transition_law_param"], "transition_law_param");
  output.push_back(migr["sojourn_law_param"], "sojourn_law_param");
  estimation_method.push_back(output, "output");
  migration.push_back(estimation_method, "estimation_method");
  return migration;
}



