#include <Rcpp.h>

#include "get_observation_probability_C.h"


// [[Rcpp::export]]
double get_observation_probability_C(
    const Rcpp::List& migration, 
    const Rcpp::NumericMatrix& hidden_count, 
    bool use_log)
{
  Rcpp::NumericMatrix obs = migration["observation"];
  double ret = 1;
  if (use_log) {
    ret = 0;
  }
  for (int t=0; t<obs.ncol(); t++){
    if (use_log) {
      ret += get_observation_probability_t(migration, obs, 
                                           hidden_count, t, use_log);
    } else {
      ret = ret* get_observation_probability_t(migration, obs, 
                                               hidden_count, t, use_log);
    }
  }
  return ret;
}

// [[Rcpp::export]]
Rcpp::NumericMatrix get_observation_probability_decomposed_C(
    const Rcpp::List& migration, 
    const Rcpp::NumericMatrix& hidden_count, 
    bool use_log)
{
  Rcpp::NumericMatrix obs = migration["observation"];
  bool opt_negbin = false;
  double negbin_delta = 0;
  double negbin_p = 0;
  if (Rcpp::as<std::string>(migration["observation_law_type"]) != "Poisson"){
    opt_negbin = true;
    Rcpp::List  obs_params = migration["observation_law_param"];
    negbin_delta = obs_params["delta"];
    negbin_p= obs_params["p"];
  }
  
  Rcpp::NumericMatrix decomposed(obs.rows(), obs.ncol());
  std::fill(decomposed.begin(), decomposed.end(), 
            Rcpp::NumericMatrix::get_na()) ;
  for (int t=0; t<obs.ncol(); t++){
    for (int i=0; i<obs.rows(); i++) {
      double obs_it = obs(i, t);
      if (not std::isnan(obs_it)) {
        double hidden_count_it = hidden_count(i, t);
        double pit = get_observation_probability_it(
          obs_it, hidden_count_it, opt_negbin, negbin_delta, negbin_p, use_log);
        decomposed(i, t) = pit;
      }
    }
  }
  return decomposed;
}



/**
 * Computes the probability of observations given the birds count 
 * at time t to compute weights. See document p.9 : P(O | Pi_t^m)
 * 
 * @param [IN] obs : Matrix of observed bird counts
 *   (I numbers of sites, plus the virtual sites death and flight)
 * @param[IN] hidden_counts: Matrix of simulated bird counts 
 * @param[IN] timestep: timestep at which to compute the probability
 * @param[IN] use_log, should the log be returned.
 * @return probability of the observed data given simulated counts at time t.
 */
double get_observation_probability_t(const Rcpp::List& migration,
                                     const Rcpp::NumericMatrix& obs, 
                                     const Rcpp::NumericMatrix& hidden_count,
                                     int timestep,
                                     bool use_log)
{
  bool opt_negbin = false;
  double negbin_delta = 0;
  double negbin_p = 0;
  if (Rcpp::as<std::string>(migration["observation_law_type"]) != "Poisson"){
    opt_negbin = true;
    Rcpp::List  obs_params = migration["observation_law_param"];
    negbin_delta = obs_params["delta"];
    negbin_p= obs_params["p"];
  }
  double ret = 1;
  if (use_log) {
    ret = 0;
  }
  for (int i=0; i<obs.rows(); i++) {
    double obs_it = obs(i, timestep);
    if (not std::isnan(obs_it)) {
      double hidden_count_it = hidden_count(i, timestep);
      double pit = get_observation_probability_it(
        obs_it, hidden_count_it, opt_negbin, negbin_delta, negbin_p, use_log);
      if (use_log) {
        ret += pit;
      } else {
        ret = ret*pit;
      }
    }
  }
  return ret;
}

/**
 * Computes the probability of observations given the birds count 
 * at time t on site i.
 * 
 * @param[IN] obs_it : double value of the observation on site i at time t
 * @param[IN] hidden_count_it: birds count on site i at time t
 * @param[IN] opt_negbin: if TRUE, the NegBin is used to model obs
 * @param[IN] negbin_delta: delta parameter of the NegBin model if necessary
 * @param[IN] negbin_p: p parameter of the NegBin model if necessary
 * @param[IN] use_log, should the log be returned.
 * @return probability of the observed data given counts at time t on site i.
 */
double get_observation_probability_it(double obs_it,
                                      double hidden_count_it, 
                                      bool opt_negbin,
                                      double negbin_delta,
                                      double negbin_p,
                                      bool use_log)
{
  if (hidden_count_it == 0)  {
    return(R::dnbinom(obs_it, 1, 0.8, use_log));
  } else if (opt_negbin){
    double negbin_r = negbin_delta*hidden_count_it*negbin_p/(1-negbin_p);
    return(R::dnbinom(obs_it, negbin_r, negbin_p, use_log));
  } else {
    return(R::dpois(obs_it, hidden_count_it, use_log));
  }
}

