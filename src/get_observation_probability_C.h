
#ifndef RCPP_BIRDNET_CPP_GET_OBSERVATION_PROBABILITY
#define RCPP_BIRDNET_CPP_GET_OBSERVATION_PROBABILITY

#include <Rcpp.h>



// Computes the probability of observations given the birds count from 
// trajectories : P(O | Pi_[0:T])
double get_observation_probability_C(
    const Rcpp::List& migration,
    const Rcpp::NumericMatrix& hidden_count, 
    bool use_log);

// Computes the probability of each observation given the birds count from 
// trajectories : P(O | Pi_[0:T])
Rcpp::NumericMatrix get_observation_probability_decomposed_C(
    const Rcpp::List& migration, 
    const Rcpp::NumericMatrix& hidden_count, 
    bool use_log);


// Computes the probability of observations given the birds count 
// at time t to compute weights.
double get_observation_probability_t(const Rcpp::List& migration,
                                     const Rcpp::NumericMatrix& obs, 
                                     const Rcpp::NumericMatrix& hidden_count,
                                     int timestep,
                                     bool use_log);


// Computes the probability of observations given the birds count 
// at time t on site i.
double get_observation_probability_it(double obs_it,
                                      double hidden_count_it, 
                                      bool opt_negbin,
                                      double negbin_delta,
                                      double negbin_p,
                                      bool use_log);


#endif
