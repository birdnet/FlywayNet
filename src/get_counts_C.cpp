#include <Rcpp.h>

#include "get_counts_C.h"

/*
 * Counts birds on each state for every time step.
 * @param [IN/OUT] count structure 
 * @param [IN] nb_particles
 * @param [IN] nb_sites, the nb of rows is then nb_sites+2
 * @param [IN] timestep, timestep for which we have to compute the counts,
 *             if -1, then counts for all timesteps should be computed
 * @param [IN] trajectories of nb_particles (nb_birs*nb_particles rows)
 */ 

void internal_compute_counts(Rcpp::NumericMatrix counts,
                             int nb_particles,
                             int nb_sites,
                             int timestep,
                             const Rcpp::NumericMatrix& particles)
{
  int nb_birds = particles.rows()/nb_particles;
  int horizon_col = particles.cols();
  int begin_t = 0;
  int end_t = horizon_col;
  if (timestep != -1) {
    begin_t = timestep;
    end_t = timestep+1;
  }
  for (int m = 0; m<nb_particles; m++) {
    for (int t = begin_t; t<end_t; t++){
      int col_to_fill = t;
      if (timestep != -1) {
        col_to_fill = 0;
      }
      for (int site=-1; site<=nb_sites;site++){
        int nbbirds = 0;
        for (int i = 0; i<nb_birds; i++){
          if (particles(m*nb_birds+i, t) == site) nbbirds ++;
        }
        if (site==-1) {
          counts(m*(nb_sites+2)+nb_sites, col_to_fill) = nbbirds;
        } else if (site == 0) {
          counts(m*(nb_sites+2)+nb_sites+1, col_to_fill) = nbbirds;
        } else {
          counts(m*(nb_sites+2)+site-1, col_to_fill) = nbbirds;
        }
      }
    }
  }
}

// [[Rcpp::export]]
Rcpp::NumericMatrix get_counts_C (Rcpp::List migration, 
                                  Rcpp::NumericMatrix trajs)
{
  const Rcpp::StringVector& site_name = migration["site_name"];
  int nb_sites = site_name.length();
  int horizon_col = Rcpp::as<int>(migration["horizon"]) + 1 ;
  Rcpp::NumericMatrix counts(nb_sites+2, horizon_col);
  internal_compute_counts(counts, 1, nb_sites, -1, trajs);
  return counts;
}


/*
 *  Count birds on each site for a specific time step in a trajectory.
 * 
 * @param [IN] migration, a migration structure
 * @param [IN] trajs, trajectories of individual birds 
 * @param [IN] timestep, timestep for which we have to compute the counts,
 * @return a vector of bird counts of size site_nb + 2.
 * index site_nb + 1 gives the count of flying birds
 * index site_nb + 2 gives the count of dead birds
*/
Rcpp::NumericVector get_counts_t (const Rcpp::List& migration, 
                                  Rcpp::NumericMatrix trajs,
                                  int timestep)
{
  const Rcpp::StringVector& site_name = migration["site_name"];
  int nb_sites = site_name.length();
  Rcpp::NumericMatrix counts(nb_sites+2, 1);
  internal_compute_counts(counts, 1, nb_sites, timestep, trajs);
  return counts.column(0);
}

/*
 *  Counts birds on each state for every time step for multiple particles
 *  Count matrices are stacked.
 * 
 * @param [IN] migration, a migration structure
 * @param [IN] trajs, trajectories of individual birds
 */
Rcpp::NumericMatrix get_counts_particles_C (const Rcpp::List& migration, 
                                  Rcpp::NumericMatrix particles,
                                  int nb_particles)
{
  const Rcpp::StringVector& site_name = migration["site_name"];
  int nb_sites = site_name.length();
  int horizon_col = Rcpp::as<int>(migration["horizon"]) + 1 ;
  Rcpp::NumericMatrix counts(nb_particles*(nb_sites+2), horizon_col);
  internal_compute_counts(counts, nb_particles, nb_sites, -1, particles);
  return counts;
}

