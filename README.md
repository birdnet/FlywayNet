---
output:
  pdf_document: default
  html_document: default
---
# FlywayNet R package

A package to infer the structure of migratory bird flyway networks from observed count data. 

For further information about the package, see 
"Nicol S, Cros MJ, Peyrard N, Sabbadin R, Trépos R, Fuller R, Woodworth B (2022). FlywayNet: A hidden 
semi-Markov model for inferring the structure of migratory bird networks. Manuscript submitted to Methods in Ecology and Evolution May 2022"

A vignette containing a minimal worked example is included with this package, see "vignettes/introduction.Rmd".

## Code

Codes are shared on the gitlab forgemia (MathNum - INRAE).
See https://forgemia.inra.fr/birdnet/FlywayNet

## Install the R package
To install the package, type the following commands into the R console.

`
library(remotes); 
install_gitlab(repo="birdnet/FlywayNet@master", host="https://forgemia.inra.fr/") 
`


## Check sources and build the manual

To check the package and build the manual:

`
library(roxygen2)
library(devtools)
options(cli.hyperlink=FALSE)  # bug in cli https://github.com/r-lib/cli/issues/441
roxygenize("FlywayNet")
devtools::check("FlywayNet")
devtools::build_manual("FlywayNet")
`


## Project members (alphabetic order)

Marie-Josée Cros (INRAE, France)
Sam Nicol (CSIRO, Australia)
Nathalie Peyrard (INRAE, France)
Régis Sabbadin (INRAE France)
Ronan Trépos (INRAE France)
